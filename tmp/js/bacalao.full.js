
/* jshint strict: true, browser: true */
/* globals define */

define('bacalao',['jQuery'], function() {
  function Bacalao() {}

  return Bacalao;
});


/* jshint strict: true */
/* globals document, window, require */

(function(require) {
  var requireConfig = {};
  if (typeof jQuery === 'undefined') {
    // If jQuery is not defined load it from Google CDN.
    requireConfig.jQuery =
      'https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js';
  }

  require.config(requireConfig);

  require(['bacalao'], function(Bacalao) {
    new Bacalao();
  });
})(require);

define("main", function(){});

