'use strict';
/* jshint strict: true, browser: true */
/* globals define */

define(['exports'], function(exports) {
  /**
   * Trims 'px' from CSS size strings like 10px and converts it to number.
   * @param s {string}
   * @returns {Number}
   */
  exports.pixelToNumber = function(s) {
    var idx = s.indexOf('px');
    return parseFloat(idx < 0 ? s : s.substr(0, idx));
  };

  exports.isLocalStorageSupported = function() {
    try {
      return 'localStorage' in window && window.localStorage !== null;
    } catch (e) {
      return false;
    }
  };
});
