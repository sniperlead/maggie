'use strict';
/* jshint strict: true, browser: true */
/* globals define, window, document */

define(['jquery', 'config', 'logger'], function($, config, logger) {
  function Popup(ctrl, options) {
    this.idPrefix = 'sniperlead-';
    this.opts = options;

    this.ctrl = ctrl;
    this.$overlay = null;
    this.$modal = null;
    this.$form = null;
    this.$phoneNumber = null;
  }

  Popup.prototype.addCSS = function() {
    var deferred = $.Deferred();
    var id = this.idPrefix + 'css';
    var head = $('head');
    if ($('#' + id, head).length !== 0) {
      return deferred.resolve();
    }
    var link  = $('<link/>')
      .attr('id', id)
      .attr('rel', 'stylesheet')
      .attr('type', 'text/css')
      .attr('href', config.host + '/static/css/popup.min.css')
      .attr('media', 'all');
    head.append(link);
    link[0].onload = function() {
      deferred.resolve();
    };
    return deferred;
  };

  /**
   * Shows popup window.
   */
  Popup.prototype.show = function() {
    this.addCSS().then(function() {
      this.render();
      this.$overlay.fadeIn('fast', function() {
        this.$modal.fadeIn();
      }.bind(this));
    }.bind(this));
  };

  /**
   * Builds HTML for popup and appends it to BODY.
   */
  Popup.prototype.render = function() {
    var body = $('body');

    if (!this.$overlay) {
      this.$overlay = $('<div></div>').attr('id', this.idPrefix + 'overlay');
      body.append(this.$overlay);
      this.$overlay.click(this.close.bind(this));
    }

    if (!this.$modal) {
      this.$modal = $('<div></div>').attr('id', this.idPrefix + 'modal');

      var inner = $('<div></div>').addClass('inner');
      this.$form = $('<form></form>');
      this.$form.on('submit', this.onSubmit.bind(this));

      var head = $('<div></div>')
        .addClass('form-row')
        .addClass('head')
        .append(
        $('<div></div>')
          .addClass('picture')
          .append($('<img/>').attr('src', 'img/picture.jpg')))
        .append(
        $('<div></div>')
          .addClass('form-title')
          .text('Извините, мы сейчас не в офисе. Мы свяжемся с Вами в ' +
          'ближайшее время. Во сколько Вам позвонить?'));

      this.$phoneNumber = $('<input/>')
        .attr('type', 'text')
        .attr('maxlength', 15)
        .attr('placeholder', '+7 123 456-78-90');

      var submit = $('<input/>')
        .attr('type', 'submit')
        .attr('value', 'Жду звонка');

      var inputs = $('<div></div>').addClass('form-row');
      inputs
        .append(this.$phoneNumber)
        .append(submit);

      this.$form
        .append(head)
        .append(inputs);

      inner.append(this.$form);
      this.$modal.append(inner);

      body.append(this.$modal);
    }
  };

  Popup.prototype.destroy = function() {
    logger.log('Destroy popup');
    this.$overlay.remove();
    this.$modal.remove();
  };

  Popup.prototype.close = function() {
    logger.log('Close popup');
    this.$modal.hide();
    this.$overlay.hide();
    this.destroy();
  };

  Popup.prototype.onSubmit = function(e) {
    e.preventDefault();
    logger.group('Popup form submit');

    var isValid = false;
    var data = {};

    data.phone_number = $('input', this.$form).val();
    // TODO: strip phone number
    if (!data.phone_number) {
      isValid = true;
      logger.log('Empty phone number');
      // TODO: show error
    } else if (/^[0-9]{11,15}$/.test(data.phone_number)) {
      isValid = true;
      logger.log('Invalid phone number');
      // TODO: show error
    }
    logger.groupEnd();

    if (!isValid) {
      return;
    }

    this.ctrl.createCallback(data);

    return false;
  };

  return Popup;
})
;
