'use strict';
/* jshint strict: true, browser: true */
/* globals define, location, localStorage */

define([
  'jquery',
  'jquery.cookie',
  'logger',
  'utils',
  'config',
  'rules',
  'popup'
], function($, jqueryCookie, logger, utils, config, Rules, Popup) {
  function Maggie() {
    this.server = 'http://sniperlead.com';
    this.serverTimeout = 5000;  // 5 s
    this.requestId = undefined;
    this.keyPrefix = 'sniperlead-maggie-';
    this.uri = location.href;
    this.referer = location.origin;
    this.clientId = this.getClientId() || '';
    this.rules = [];
    this.points = 0;
  }

  Maggie.prototype.getClientId = function() {
    var key = this.keyPrefix + 'client_id';
    if (utils.isLocalStorageSupported()) {
      return localStorage.getItem(key);
    } else {
      return $.cookie(key);
    }
  };

  Maggie.prototype.saveClientId = function(clientId) {
    var key = this.keyPrefix + 'client_id';
    if (utils.isLocalStorageSupported()) {
      localStorage.setItem(key, clientId);
    } else {
      $.cookie(key, clientId, 365);
    }
    this.clientId = clientId;
  };

  Maggie.prototype.addPoints = function(points) {
    logger.debug('Add points', points);
    this.points += points;
    if (this.points >= 100) {
      this.showPopup();
    }
  };

  Maggie.prototype.showPopup = function() {
    this.popup = new Popup(this);
    this.popup.show();
  };

  Maggie.prototype.executeRules = function(rulesOpts) {
    rulesOpts = rulesOpts || {};
    var Rule;
    var rule;
    for (var ruleName in rulesOpts) {
      if (rulesOpts.hasOwnProperty(ruleName)) {
        Rule = Rules[ruleName];
        if (!Rule) {
          logger.warn('Unknown rule type:', ruleName, rulesOpts[ruleName]);
          continue;
        }
        rule = new Rule(rulesOpts[ruleName]);
        logger.debug('Executing rule:', rule.toString());
        rule.execute().then(this.addPoints.bind(this));
        this.rules.push(rule);
      }
    }
  };

  Maggie.prototype.getAjaxOptions = function() {
    return {
      method: 'POST',
      timeout: this.serverTimeout,
      contentType: 'application/json; charset=utf-8',
      async: true,
      cache: false,
      dataType: 'json',
      context: this
    };
  };

  Maggie.prototype.initSession = function() {
    logger.group('Starting session');
    var data = {
      uri: this.uri,
      referer: this.referer
    };
    if (this.clientId) {
      data.client_id = this.clientId;
    }
    var opts = this.getAjaxOptions();
    opts.data = JSON.stringify(data);
    return $.ajax(config.host + '/r', opts)
      .then(function(data) {
        this.requestId = data.id;
        logger.log('Request id:', this.requestId);

        if (data.client_id) {
          this.saveClientId(data.client_id);
          logger.log('Client id:', data.client_id);
        }

        this.executeRules(data.rules || {});
      }.bind(this))
      //.then(function() {
      //  this.showPopup();
      //}.bind(this))
      .then(function() {
        logger.groupEnd();
      });
  };

  Maggie.prototype.createCallback = function(data) {
    data.request_id = this.requestId;
    logger.group('Creating callback {request_id: %s, phone_number: %s}',
      this.requestId, data.phone_number);
    logger.log('data:', data);
    var opts = this.getAjaxOptions();
    opts.data = JSON.stringify(data);
    $.ajax(config.host + '/cb', opts)
      .then(function(r) {
        this.popup.close();
      }.bind(this))
      .fail(function(err) {
        // TODO: show error
        logger.error(err);
      })
      .always(function() {
        logger.groupEnd();
      });
  };

  Maggie.prototype.suck = function() {
    this.initSession();
  };

  return Maggie;
});
