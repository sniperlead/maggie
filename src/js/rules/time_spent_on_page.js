'use strict';
/* jshint strict: true, browser: true */
/* globals define */

define(['jquery', 'logger'], function($, logger) {
  function TimeSpentOnPageRule(opts) {
    this.points = opts.points || 0;
    this.duration = (parseInt(opts.spent_more_then) || 0) * 1000;
  }

  TimeSpentOnPageRule.prototype.toString = function() {
    return '[TimeSpentOnPageRule points: ' + this.points + ' duration: ' +
      this.duration +']';
  };

  TimeSpentOnPageRule.prototype.execute = function() {
    var deferred = $.Deferred();
    setTimeout(function() {
      logger.group('User spent more than ' + this.duration + ' on page');
      deferred.resolve(this.points).then(function() {
        logger.groupEnd();
      });
    }.bind(this), this.duration);
    return deferred;
  };

  return TimeSpentOnPageRule;
});