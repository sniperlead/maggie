'use strict';
/* jshint strict: true */
/* globals document, window, requirejs, jQuery, define */

((function(requirejs) {
  var requireConfig = {paths: {}};
  if (typeof jQuery === 'undefined') {
    // If jQuery is not defined load it from Google CDN.
    requireConfig.paths.jquery =
      'https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min';
  } else {
    define('jquery', function() {
      return jQuery;
    });
  }

  requirejs.config(requireConfig);

  requirejs(['sniper'], function(Maggie) {
    (new Maggie()).suck();
  });
})(requirejs));
