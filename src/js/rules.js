'use strict';
/* jshint strict: true, browser: true */
/* globals define */

define(['rules/time_spent_on_page'], function(TimeSpentOnPageRule) {
  return {
    time_spent_on_page: TimeSpentOnPageRule
  };
});