'use strict';
/* jshint strict: true, browser: true */
/* globals define */

define('config', function() {
  return {
    host: 'http://sniperlead.com',
    isProd: true
  };
});