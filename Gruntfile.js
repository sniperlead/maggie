'use strict';
/* jshint strict: true, node: true */

var path = require('path');
var util = require('util');
var _ = require('underscore');

function here() {
  var args = Array.prototype.slice.call(arguments, 0);
  args.unshift(__dirname);
  return path.join.apply(path.join, args);
}

module.exports = function(grunt) {
  grunt.task.loadNpmTasks('grunt-contrib-jshint');
  grunt.task.loadNpmTasks('grunt-bower-task');
  grunt.task.loadNpmTasks('grunt-contrib-concat');
  grunt.task.loadNpmTasks('grunt-contrib-cssmin');
  grunt.task.loadNpmTasks('grunt-contrib-less');
  grunt.task.loadNpmTasks('grunt-contrib-uglify');
  grunt.task.loadNpmTasks('grunt-contrib-requirejs');
  grunt.task.loadNpmTasks('grunt-contrib-watch');

  var pkg = grunt.file.readJSON(here('package.json'));

  var paths = {
    jquery: '../../vendor/jquery',
    'jquery.cookie': '../../vendor/jquery.cookie'
  };

  grunt.initConfig({
    pkg: pkg,
    bower: {
      install: {
        options: {
          targetDir: '',
          verbose: true,
          layout: function() {
            return 'vendor';
          }
        }
      }
    },
    concat: {
      prod: {
        src: [
          here('vendor', 'require.js'),
          here('config.js'),
          here('src', 'js', 'main.js'),
          here('dist', 'js', 'sniper.js')
        ],
        dest: here('dist', 'js', 'sniper.js')
      }
    },
    jshint: {
      files: [
        'src/js/**/*.js',
        'test/**/*.js',
        'Gruntfile.js'
      ],
      options: {
        strict: true,
        node: true,
        indent: 2,
        maxlen: 80
      }
    },
    less: {
      main: {
        expand: true,
        cwd: here('src', 'less'),
        src: '**/*.less',
        dest: here('tmp', 'css'),
        ext: '.css'
      }
    },
    cssmin: {
      main: {
        files: {
          'dist/css/popup.min.css': 'tmp/css/popup.css'
        }
      }
    },
    uglify: {
      options: {
        beautify: false
      },
      dist: {
        src: here('dist', 'js', 'sniper.js'),
        dest: here('dist', 'js', 'sniper.min.js')
      }
    },
    watch: {
      js: {
        files: [
          'src/js/*.js',
          'vendor/*.js'
        ],
        tasks: ['js']
      },
      css: {
        files: [
          'src/less/*.less'
        ],
        tasks: ['less']
      }
    },
    requirejs: {
      prod: {
        options: {
          baseUrl: 'src/js',
          optimize: 'none',
          name: 'sniper',
          out: 'dist/js/sniper.js',
          exclude: [
            'jquery',
            'config'
          ],
          paths: {
            jquery: '../../vendor/jquery',
            'jquery.cookie': '../../vendor/jquery.cookie'
          }
        }
      }
    }
  });

  grunt.registerTask('js', [
    'jshint',
    'requirejs:prod',
    'concat:prod',
    'uglify'
  ]);

  grunt.registerTask('css', [
    'less',
    'cssmin'
  ]);

  grunt.registerTask('default', [
    'bower',
    'css',
    'js'
  ]);
};
